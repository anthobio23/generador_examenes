from django.contrib import admin
from empleados.Apps.Gestiondexamenes import Empleado, Examen, Matricula

admin.site.register(Empleado)
admin.site.register(Examen)
admin.site.register(Matricula)