## Aplicacion web
## Formularios y base d datos con django para generar examenes automatizados
## la aplicacion basada solo para el administrador

from django.db import models

class Empleado(models.Model): ## Caracteristicas del empleado a presentar
   ApellidoPaterno = models.CharField(max_length = 35)
   ApellidMaterno = models.CharField(max_length = 35)
   Nombres = models.CharField(max_length = 35)
   dni = models.Charfield(max_length=8)
   FechadeNacimiento = models.DateField()
   Sexos = (('F', 'Femenino'), ('M', 'Masculino'))
   Sexo = models.CharField(max_length=1, choices=Sexos, default='M')
   
   def NombreCompleto(self): 
      cadena = "{0} {1}, {2}"
      return cadena.format(self.ApellidoPaterno, self.ApellidMaterno, self.Nombres)
      pass
   
   def __str__(self):
      return self.NombreCompleto()
      pass
   pass

class Examen(models.Model): ## Tipo de examen a evaluar, 
   Nombre = models.CharField(max_length=50) ## creditos validados para cadad 
   creditos = models.PositiveSmallIntegerField() ##examen y estado del empleado (activo/inactivo)
   Estado = models.BooleanField(default=True)
   
   def __str__(self):
      return "{0} ({1})".format(self.Nombre, self.Creditos)
      pass
   pass

class Matricula(self): ## examen a seleccionar el empleado
   Empleado = models.ForeignKey(Empleado, null=False, blank=False, on_delete=models.CASCADE)
   Examen = models.ForeignKey(Empleado, null=False, blank=False, on_delete=models.CASCADE)
   FechaMatricula = models.DateField(auto_now_add=True)
   
   def __str__(self):
      cadena = "{0} => {1}"
      return cadena.format(self.Empleado, self.Examen.Nombre)
      pass
   pass

   
   